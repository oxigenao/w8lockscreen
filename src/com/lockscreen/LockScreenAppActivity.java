package com.lockscreen;

import java.util.Date;
import java.util.GregorianCalendar;

import receiver.lockScreenReeiver;
import android.R.string;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.KeyguardManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.PowerManager;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextPaint;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ImageView.ScaleType;
import android.widget.RelativeLayout.LayoutParams;

/**
 * @author Pablo
 *
 */
public class LockScreenAppActivity extends Activity {

	/** Called when the activity is first created. */
	KeyguardManager.KeyguardLock k1;
	boolean inDragMode;

	int windowwidth;
	int windowheight;
	public int veces = 0;
	public float x = 0;
	public float y = 0;

	public boolean pass_correcta = false;
	public TextView hora;
	public Button Ajustes;
	public ImageButton ajustes2;
	public TextView bateria;
	public ImageView foto;
	public Animation jump1;
	public SharedPreferences prefs;
	public String tipo_bloueo;
	public TextView fech;
	public Typeface font;

	public String password;
	public GregorianCalendar Calendar;
	public int h;
	public int m;
	public int dia;
	public int mes;
	public int ano;
	public EditText entrada;

	public boolean next = false;
	boolean tengoPatron = false;
	public float posX1;
	public float posY1;
	public float posX2;
	public float posY2;
	public float posX3;
	public float posY3;
	public Boolean EstadoBateria;

	private BroadcastReceiver batteryLevelReceiver;
	private BroadcastReceiver HoraChange;
	public Intent inten;

	@Override
	public void onAttachedToWindow() {
		// TODO Auto-generated method stub
		this.getWindow().setType(
				WindowManager.LayoutParams.TYPE_KEYGUARD_DIALOG
						| WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN); // pone
		// en
		// modo
		// full
		// screen

		super.onAttachedToWindow();
	}

	public void onCreate(Bundle savedInstanceState) {

		Log.d("Estadodelapantalla", "SE pone en CREATE");
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		inten = getIntent();
		EstadoBateria = (Boolean) inten.getBooleanExtra("bateria", false);

		getWindow().addFlags(
				WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
						| WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);

		windowwidth = getWindowManager().getDefaultDisplay().getWidth();
		windowheight = getWindowManager().getDefaultDisplay().getHeight();
		font = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Light.ttf");

		fech = (TextView) findViewById(R.id.fecha);
		hora = (TextView) findViewById(R.id.Horas);
		bateria = (TextView) findViewById(R.id.battery_text);
		jump1 = AnimationUtils.loadAnimation(this, R.anim.jump);

		ajustes2 = (ImageButton) findViewById(R.id.ajuste2);

		// -------------------------------------------------------------------------------------------------------------------------------------------------------
		// --------------------------COMPROBACIONES
		// INICALES------------------------------------------------------------------------------------------------------

		onFirstRun();
		// serviciooapp();
		preferencias();
		ActualizaBateria();

		// el rollo de la contraseņa en lso ajustes

		// --------------------------------------------------------------------

		ajustes2.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {

				PonPassword();

			}
		});

		if (getIntent() != null && getIntent().hasExtra("kill")
				&& getIntent().getExtras().getInt("kill") == 1) {
			// Toast.makeText(this, "" + "kill activityy",
			// Toast.LENGTH_SHORT).show();
			// finish();
		}

		// ----------------------------------------------------------------------------------------------------------------------------------------------------------
		// ------------------------------------LA ZONA
		// BASICA-------------------------------------------------------------------------------------------
		try {

			// MANTENEMOS UN ESRVICIO ACTIVO PARA LAS LLAMADAS
			startService(new Intent(this, MyService.class)); // iniciamos el
																// servicio
			StateListener phoneStateListener = new StateListener();
			TelephonyManager telephonyManager = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
			telephonyManager.listen(phoneStateListener,
					PhoneStateListener.LISTEN_CALL_STATE);

			actualizarhora();

		} catch (Exception e) {
			Log.d("pito", "MUERTEEEEEEEEEEEEEEEEE");
		}

	}

/*	private void serviciooapp() {

		if (inten.getIntExtra("servicio", 3) == 3) {
			Log.d("Estadodelapantalla", "NO VIENE DE SERVICIO");
			Intent set = new Intent(LockScreenAppActivity.this,
					SettingActivity.class);

			set.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			set.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(set);

		}

	}*/

	public void PonPassword() {

		entrada = new EditText(this);
		entrada.setInputType(1);
		final AlertDialog pass = new AlertDialog.Builder(this).create();
		pass.setTitle("Contraseņa");

		pass.setView(entrada);
		pass.setButton("Aceptar", new DialogInterface.OnClickListener() {

			public void onClick(DialogInterface dialog, int which) {

				if (entrada.getText().toString().equals(password)) {
					pass_correcta = true;
					Intent settingIntent = new Intent(
							LockScreenAppActivity.this, SettingActivity.class);

					// si al contraseņa es correcta

					settingIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					settingIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					startActivity(settingIntent);
				}

			}
		});

		pass.show();
	}

	public void ActualizaBateria() {

		/*
		 * batteryLevelReceiver = new BroadcastReceiver() { public void
		 * onReceive(Context context, Intent intent) {
		 * context.unregisterReceiver(this); int rawlevel =
		 * intent.getIntExtra("level", -1); int scale =
		 * intent.getIntExtra("scale", -1); int level = -1; if (rawlevel >= 0 &&
		 * scale > 0) { level = (rawlevel * 100) / scale; }
		 * 
		 * if (EstadoBateria) {
		 * 
		 * bateria.setText("Cargando"); } else {
		 * 
		 * bateria.setText(level + "%"); } } }; IntentFilter batteryLevelFilter
		 * = new IntentFilter( Intent.ACTION_BATTERY_CHANGED);
		 * registerReceiver(batteryLevelReceiver, batteryLevelFilter);
		 */
	}

	public void actualizarhora() {

		Calendar = new GregorianCalendar();

		h = Calendar.get(GregorianCalendar.HOUR_OF_DAY);
		m = Calendar.get(GregorianCalendar.MINUTE);

		if (m / 10 <= 0) {
			hora.setText(h + "" + ":" + "0" + m + "");
		} else {
			hora.setText(h + "" + ":" + m + "");
		}

		dia = Calendar.get(GregorianCalendar.DAY_OF_MONTH);
		mes = Calendar.get(GregorianCalendar.MONTH);
		ano = Calendar.get(GregorianCalendar.YEAR);

		fech.setText(dia + "" + "/" + mes + "" + "/" + ano + "");
		fech.setTypeface(font);

		// como en on resuemen llamo a esto no hace falta que este siempre
		// atento

		/*
		 * HoraChange = new BroadcastReceiver() { public void onReceive(Context
		 * context, Intent intent) { context.unregisterReceiver(this);
		 * 
		 * Log.d("Estadodelapantalla", "Cambia la hora");
		 * 
		 * h = Calendar.get(GregorianCalendar.HOUR_OF_DAY); m =
		 * Calendar.get(GregorianCalendar.MINUTE);
		 * 
		 * if (m / 10 <= 0) { hora.setText(h + "" + ":" + "0" + m + ""); } else
		 * { hora.setText(h + "" + ":" + m + ""); }
		 * 
		 * } };
		 * 
		 * IntentFilter HoraChangeFilter = new IntentFilter(
		 * Intent.ACTION_TIME_CHANGED); registerReceiver(HoraChange,
		 * HoraChangeFilter);
		 */

	}

	class StateListener extends PhoneStateListener {
		@Override
		public void onCallStateChanged(int state, String incomingNumber) {

			super.onCallStateChanged(state, incomingNumber);
			switch (state) {
			case TelephonyManager.CALL_STATE_RINGING:
				break;
			case TelephonyManager.CALL_STATE_OFFHOOK: // si entra llamde
														// cerramos app finish()
				System.out.println("call Activity off hook");
				finish();

				break;
			case TelephonyManager.CALL_STATE_IDLE:
				break;
			}
		}
	};

	// cojo las preferencas de puntos guardados y la imagen de fondo

	public void preferencias() {

		foto = (ImageView) findViewById(R.id.fondo);
	//	foto.setImageDrawable(getResources().getDrawable(R.drawable.koala));

		prefs = getSharedPreferences("lock", 0);

		String fondo_pref = prefs.getString("fondo", "null");

		Uri myUri = Uri.parse(fondo_pref);

		if (!fondo_pref.equals("null")) {

			try {

				// ___________________________________________________________________________________________________________
				// AQUI PETAAAAAAAAA

				// ___________________________________________________________________________________________________________

				// por lo que sea esto no sale =(

			
					foto.setImageURI(myUri);
					foto.setScaleType(ScaleType.CENTER_CROP);
				//	foto.setScaleX(foto.getContentSize().width / windowwidth);
					//foto.setScaleY(foto.getContentSize().height / windowheight);
				

			}

			catch (Exception a) {
				foto.setImageDrawable(getResources().getDrawable(
						R.drawable.koala));
				foto.setScaleType(ScaleType.CENTER_CROP);
				// ssi ai algun erro deberia mostrarlo en la pantalla
			}
		}

		posX1 = prefs.getFloat("x1", 0);
		posY1 = prefs.getFloat("y1", 0);
		posX2 = prefs.getFloat("x2", 0);
		posY2 = prefs.getFloat("y2", 0);
		posX3 = prefs.getFloat("x3", 0);
		posY3 = prefs.getFloat("y3", 0);

		password = prefs.getString("pass", "1234");

		if (posX1 != 0 && posX2 != 0 && posX3 != 0 && posY1 != 0 && posY2 != 0
				&& posY3 != 0) {
			tengoPatron = true;
		} else {
			tengoPatron = false;
		}

		tipo_bloueo = prefs.getString("tipo", "arrastra");

	}

	@Override
	public boolean onKeyDown(int keyCode, android.view.KeyEvent event) {

		if ((keyCode == KeyEvent.KEYCODE_VOLUME_DOWN)
				|| (keyCode == KeyEvent.KEYCODE_POWER)
				|| (keyCode == KeyEvent.KEYCODE_VOLUME_UP)
				|| (keyCode == KeyEvent.KEYCODE_CAMERA)) {
			// this is where I can do my stuff
			return true; // because I handled the event
		}
		if ((keyCode == KeyEvent.KEYCODE_HOME)) {

			return true;
		}

		return false;

	}

	public boolean dispatchKeyEvent(KeyEvent event) {
		if (event.getKeyCode() == KeyEvent.KEYCODE_POWER
				|| (event.getKeyCode() == KeyEvent.KEYCODE_VOLUME_DOWN)
				|| (event.getKeyCode() == KeyEvent.KEYCODE_POWER)) {
			// Intent i = new Intent(this, NewActivity.class);
			// startActivity(i);
			return false;
		}
		if ((event.getKeyCode() == KeyEvent.KEYCODE_HOME)) {

			return true;
		}
		return false;
	}

	public boolean onTouchEvent(MotionEvent ev) {

		final int action = ev.getAction();

		switch (action) {

		case MotionEvent.ACTION_DOWN: {

			x = ev.getRawX();
			y = ev.getRawY();

			veces++;

		}

		// case MotionEvent.ACTION_MOVE:{
		// y=ev.getRawY();
		// }

		}
		// int a =1;

		if (tengoPatron) {

			if (veces == 1) {

				if (x > posX1 - 50 && x < posX1 + 50 && y > posY1 - 50
						&& y < posY1 + 50) {
					next = true;

				} else {
					next = false;
					veces = 0;
					foto.startAnimation(jump1);
				}
			}

			if (veces == 2 && next) {

				if (x > posX2 - 50 && x < posX2 + 50 && y > posY2 - 50
						&& y < posY2 + 50) {
					next = true;

				} else {
					next = false;
					veces = 0;
					foto.startAnimation(jump1);
				}
			}

			if (veces == 3 && next) {

				if (x > posX3 - 50 && x < posX3 + 50 && y > posY3 - 50
						&& y < posY3 + 50) {
					next = true;
					veces = 0;

					finish();
					overridePendingTransition(R.anim.slide_in_right,
							R.anim.slide_out_right);

				} else {
					next = false;
					veces = 0;
					foto.startAnimation(jump1);
				}
			}
		}

		else {
			LockScreenAppActivity.this.finish();
			overridePendingTransition(R.anim.slide_in_right,
					R.anim.slide_out_right);
		}

		return true;

	}

	public void onFirstRun() {

		boolean firstrun = getSharedPreferences("lock", MODE_PRIVATE)
				.getBoolean("firstrun", true);
		boolean siguiente = false;

		if (firstrun) {

			android.provider.Settings.System.putInt(getContentResolver(),
					android.provider.Settings.Secure.LOCK_PATTERN_ENABLED, 0); // 0-255

			Toast.makeText(LockScreenAppActivity.this,
					"desactivando seguridad", Toast.LENGTH_LONG);

			AlertDialog alertDialog = new AlertDialog.Builder(this).create();
			alertDialog.setTitle("Primera Vez");
			alertDialog
					.setMessage("Si esta es tu primera vez lo primero es seleccionar una imagen y un patron de desbloqueo ");
			alertDialog.setButton("Seleccionar Imagen",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							Intent b = new Intent(LockScreenAppActivity.this,
									SettingActivity.class);
							startActivity(b);

						}
					});

			alertDialog.show();

			// Save the state
			getSharedPreferences("lock", MODE_PRIVATE).edit()
					.putBoolean("firstrun", false).commit();
			siguiente = true;

		}

		if (siguiente) {

			final EditText input = new EditText(this);
			input.setInputType(1);
			AlertDialog pass = new AlertDialog.Builder(this).create();
			pass.setTitle("Seleccione una nueva contraseņa");
			pass.setMessage("Esta contraseņa le permitira cambiar el patron si lo olvida");
			pass.setView(input);
			pass.setButton("Aceptar", new DialogInterface.OnClickListener() {

				public void onClick(DialogInterface dialog, int which) {

					getSharedPreferences("lock", MODE_PRIVATE).edit()
							.putString("pass", input.getText().toString())
							.commit();
				}
			});
			pass.show();
		}

	}

	@Override
	protected void onResume() {
		Log.d("Estadodelapantalla", "SE pone en resumen");
		super.onResume();
	}

	@Override
	protected void onStart() {

		actualizarhora();
		ActualizaBateria();

		Log.d("Estadodelapantalla", "SE pone en onstart");

		super.onStart();
	}

	@Override
	protected void onStop() {

		Log.d("Estadodelapantalla", "STOP");
		super.onStop();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		Log.d("Estadodelapantalla", "SE pone en DESTROY");

		// unregisterReceiver(HoraChange);

		// unregisterReceiver(batteryLevelReceiver);

		super.onDestroy();
	}

	@Override
	protected void onPause() {
		Log.d("Estadodelapantalla", "SE pone en DESTROY");

		super.onPause();
	}

}
