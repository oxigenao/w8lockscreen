package com.lockscreen;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.WallpaperManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class SettingActivity extends Activity {

	Button select_image;
	public SharedPreferences prefs;
	boolean terminar = false;
	public Button PatronChange;
	Intent pat;
	public Button changepass;
	public TextView cabecera;
	public Button type_block;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.setting_layout);
		Typeface font = Typeface.createFromAsset(getAssets(),
				"fonts/Roboto-Light.ttf");

		cabecera = (TextView) findViewById(R.id.textView1);
		cabecera.setTypeface(font);

		select_image = (Button) findViewById(R.id.sel_img);
		select_image.setTypeface(font);
		select_image.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Change_Picture();
			}
		});

		PatronChange = (Button) findViewById(R.id.change_btn);
		PatronChange.setTypeface(font);
		PatronChange.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Change_patron();
			}
		});

		changepass = (Button) findViewById(R.id.change_pass);
		changepass.setTypeface(font);
		changepass.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {

				// cambio contraseņa

				cambiarPassword();

			}
		});

		type_block = (Button) findViewById(R.id.tipo_blo);
		type_block.setTypeface(font);
		type_block.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
			
				prefs = getSharedPreferences("lock", 0);
				
				prefs.edit()
				.putString("fondo", "null")
				.commit();
				
				
				Intent p = new Intent(SettingActivity.this,
						LockScreenAppActivity.class);

				p.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				p.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

				startActivity(p);
				
				
				
			}
		});

		if (terminar) {
			SettingActivity.this.finish();
		}

	}

	protected void cambiarPassword() {

		prefs = getSharedPreferences("lock", 0);

		final String password = prefs.getString("pass", "1234");
		
		
		final Dialog pass = new Dialog(SettingActivity.this);
		pass.setContentView(R.layout.password_dialog_layout);
		pass.setCancelable(true);
		
		
		final Button ok = (Button) pass.findViewById(R.id.ok);
		final EditText passahora = (EditText) pass.findViewById(R.id.passactual);
		final EditText passnueva = (EditText) pass.findViewById(R.id.passnueva);
		final TextView err = (TextView) pass.findViewById(R.id.error);

		//LayoutInflater inflater = this.getLayoutInflater();
	//	pass.setView(inflater.inflate(R.layout.password_dialog_layout, null));
		

		ok.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {

				if (passahora.getText().toString().equals(password)) {
					prefs.edit()
							.putString("pass", passnueva.getText().toString())
							.commit();

					Intent b = new Intent(SettingActivity.this,
							LockScreenAppActivity.class);

					b.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					b.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

					startActivity(b);
				}

				else {

					err.setText("Contraseņa Erronea Escriba una nueva");
					passahora.setText("");
					passnueva.setText("");

				}

			}
		});

		pass.show();

	}

	public void Change_patron() {

		pat = new Intent(SettingActivity.this, ChangePatron.class);

		pat.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		pat.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

		startActivity(pat);
	}

	public void Change_Picture() {
		Intent intent = new Intent(Intent.ACTION_PICK,
				android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
		startActivityForResult(intent, 0);

	}

	// activida sobre la eleccion de la imagen
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);

		if (resultCode == RESULT_OK) {
			Uri targetUri = data.getData();

			// guardo en las preferencias la direccion de la imagen de fondo

			prefs = getSharedPreferences("lock", 0);
			SharedPreferences.Editor editor = prefs.edit();
			editor.putString("fondo", targetUri.toString());
			editor.commit();

			Change_patron(); // obligo a cambiar el patron

		}
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		// super.onBackPressed();

		// muy importante este intent borra toda actividad anterior
		// empezando de nuevo

		Intent a = new Intent(SettingActivity.this, LockScreenAppActivity.class);

		a.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		a.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

		startActivity(a);
	}
}
