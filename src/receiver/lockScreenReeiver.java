package receiver;

import android.app.KeyguardManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import android.util.Log;
import android.widget.Toast;

import com.lockscreen.LockScreenAppActivity;

public class lockScreenReeiver extends BroadcastReceiver {
	public static boolean wasScreenOn = true;
	public static boolean bateryState = true; // true es cargand false normal
	public static boolean llamada = false;

	@Override
	public void onReceive(Context context, Intent intent) {

		// context.unregisterReceiver(this);

		if (intent.getAction().equals(Intent.ACTION_SCREEN_OFF)) {

			wasScreenOn = false;
			Intent intent11 = new Intent(context, LockScreenAppActivity.class);
			intent11.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			intent11.putExtra("Cargando", bateryState);
			intent11.putExtra("servicio", 1);
			context.startActivity(intent11);

			Log.d("Estadodelapantalla", wasScreenOn + "");

			// do whatever you need to do here

		} else if (intent.getAction().equals(Intent.ACTION_SCREEN_ON)) {

			wasScreenOn = true;
			Intent intent11 = new Intent(context, LockScreenAppActivity.class);
			intent11.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			intent11.putExtra("Cargando", bateryState);
			intent11.putExtra("servicio", 1);
			context.startActivity(intent11);
			Log.d("Estadodelapantalla", wasScreenOn + "");
			// wasScreenOn = true;
		}

		// cosicas de la cargando o de llamada en camino

		else if (intent.getAction().equals(intent.ACTION_POWER_CONNECTED)) {

			bateryState = true;

		} else if (intent.getAction().equals(intent.ACTION_POWER_DISCONNECTED)) {

			bateryState = false;

		}// si se recive una llamada

		else if (intent.getAction().equals(intent.ACTION_ANSWER)) {

			llamada = true;

		}

		else if (intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)) {

			/*
			 * KeyguardManager.KeyguardLock k1; KeyguardManager km
			 * =(KeyguardManager
			 * )context.getSystemService(context.KEYGUARD_SERVICE); k1 =
			 * km.newKeyguardLock("IN"); k1.disableKeyguard();
			 */

			Intent intent11 = new Intent(context, LockScreenAppActivity.class);

			intent11.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

			// context.startActivity(intent11);

			// cuadno bootea no debe salta el llock screnn pues se tiene uqe
			// poner el pin

		}

	}

}
