package com.lockscreen;

import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.Menu;
import android.view.MotionEvent;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;

public class ChangePatron extends Activity {

	public ImageView foto;
	public SharedPreferences prefs;
	public int veces;
	public float x = 0;
	public float y = 0;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_change_patron);

		foto = (ImageView) findViewById(R.id.fondo);

		prefs = getSharedPreferences("lock", 0);

		String fondo_pref = prefs.getString("fondo", "null");

		try {

			// ___________________________________________________________________________________________________________
			// AQUI PETAAAAAAAAA

			// ___________________________________________________________________________________________________________

			if (fondo_pref.equals("null")) {
				foto.setImageDrawable(getResources().getDrawable(
						R.drawable.koala));
				foto.setScaleType(ScaleType.CENTER_CROP);

			} else {
				foto.setImageURI(Uri.parse(fondo_pref));
				foto.setScaleType(ScaleType.CENTER_CROP);
				

				//foto.setScaleX( foto.getContext().width /windowwidth );
				// foto.setScaleY( foto.getContentSize().height /windowheight);
			}

		}

		catch (Exception a) {
			foto.setImageDrawable(getResources().getDrawable(R.drawable.koala));
			foto.setScaleType(ScaleType.CENTER_CROP);
			// ssi ai algun erro deberia mostrarlo en la pantalla
		}

		foto.setScaleType(ScaleType.CENTER_CROP);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_change_patron, menu);
		return true;
	}

	public boolean onTouchEvent(MotionEvent ev) {

		final int action = ev.getAction();
		prefs = getSharedPreferences("lock", 0);
		SharedPreferences.Editor editor = prefs.edit();

		switch (action) {

		case MotionEvent.ACTION_DOWN: {

			x = ev.getRawX();
			y = ev.getRawY();

			veces++;

		}

			if (veces == 1) {
				Log.d("pito", "Guardado punto1");
				editor.putFloat("x1", x);
				editor.commit();
				editor = prefs.edit();
				editor.putFloat("y1", y);
				editor.commit();

			}

			if (veces == 2) {
				Log.d("pito", "Guardado punto2");
				editor = prefs.edit();
				editor.putFloat("x2", x);
				editor.commit();
				editor = prefs.edit();
				editor.putFloat("y2", y);
				editor.commit();

			}

			if (veces == 3) {
				Log.d("pito", "Guardado punto3");
				editor = prefs.edit();
				editor.putFloat("x3", x);
				editor.commit();
				editor = prefs.edit();
				editor.putFloat("y3", y);
				editor.commit();

				// pongo un dialoog de confirmacion si sale o no

				AlertDialog alertDialog = new AlertDialog.Builder(this)
						.create();

				alertDialog.setMessage("Haz finalizado tus cambios?? ");
				alertDialog.setButton("SI",
						new DialogInterface.OnClickListener() {

							public void onClick(DialogInterface dialog,
									int which) {

								Intent a = new Intent(ChangePatron.this,
										LockScreenAppActivity.class);

								a.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
								a.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

								startActivity(a);
								Log.d("pito", "me salgo al principal");

							}
						});

				alertDialog.setButton2("VOlver a Escribir",
						new DialogInterface.OnClickListener() {

							public void onClick(DialogInterface dialog,
									int which) {

								veces = 0;

							}
						});

				alertDialog.show();

			}

		}

		return true;

	}

	@Override
	public void onBackPressed() {
		
	//	super.onBackPressed();
		Intent z = new Intent(ChangePatron.this,
				LockScreenAppActivity.class);
		z.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		z.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

		startActivity(z);
	}
	
	
}
